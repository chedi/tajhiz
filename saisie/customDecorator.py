from django.template     import RequestContext
from django.shortcuts    import render_to_response
from django.contrib.auth import REDIRECT_FIELD_NAME

from tajhiz.saisie.models import Activite

import datetime

def user_passes_test_with_403(test_func, login_url=None):
    if not login_url:
        from django.conf import settings
        login_url = settings.LOGIN_URL
    def _dec(view_func):
        def _checklogin(request, *args, **kwargs):
            if test_func(request.user):
                return view_func(request, *args, **kwargs)
            elif not request.user.is_authenticated():
                return HttpResponseRedirect('%s?%s=%s' % (login_url, REDIRECT_FIELD_NAME, quote(request.get_full_path())))
            else:
                resp = render_to_response('403.html', context_instance=RequestContext(request))
                resp.status_code = 403
                return resp
        _checklogin.__doc__  = view_func.__doc__
        _checklogin.__dict__ = view_func.__dict__
        return _checklogin
    return _dec

def permission_required_with_403(perm, login_url=None):
    return user_passes_test_with_403(lambda u: u.has_perm(perm), login_url=login_url)

def log_user_action_start(function=None, activity_name=None):
    def _dec(view_func):
        def _view(request, *args, **kwargs):
            now = datetime.datetime.now()
            activity = Activite.objects.create(user=request.user, start=now, end=now, closed=False, action=activity_name)
            activity.save()
            return view_func(request, *args, **kwargs)

        _view.__name__ = view_func.__name__
        _view.__dict__ = view_func.__dict__
        _view.__doc__  = view_func.__doc__
        return _view

    if function is None:
        return _dec
    else:
        return _dec(function)

def log_user_action_end(function=None, activity_name=None):
    def _dec(view_func):
        def _view(request, *args, **kwargs):
            activity_not_found_em = u"can't find an activity matching the requested filters (log_user_action_end)"
            now = datetime.datetime.now()
            open_activities = Activite.objects.filter(user=request.user).filter(action=activity_name).filter(closed=False)
            if open_activities.count == 0:
                resp = render_to_response('501.html', { custom_error_message : activity_not_found_em }, context_instance=RequestContext(request))
                resp.status_code = 501
                return resp
            elif open_activities.count > 1:
                for i in open_activities:
                    i.end    = now
                    i.closed = True
                    i.save()
            else:
                open_activities[0].end    = now
                open_activities[0].closed = True
                open_activities[0].save()
            return view_func(request, *args, **kwargs)

        _view.__name__ = view_func.__name__
        _view.__dict__ = view_func.__dict__
        _view.__doc__  = view_func.__doc__
        return _view

    if function is None:
        return _dec
    else:
        return _dec(function)