INSERT INTO saisie_categoriepanneau (id, label, parent_id, lft, rght, tree_id, level) VALUES (22, 'Indication Type C', 21, 2, 3, 3, 1);
INSERT INTO saisie_categoriepanneau (id, label, parent_id, lft, rght, tree_id, level) VALUES (21, 'Indications', NULL, 1, 6, 3, 0);
INSERT INTO saisie_categoriepanneau (id, label, parent_id, lft, rght, tree_id, level) VALUES (23, 'Indication Type CE', 21, 4, 5, 3, 1);
INSERT INTO saisie_categoriepanneau (id, label, parent_id, lft, rght, tree_id, level) VALUES (4, 'Danger Type A', 3, 2, 3, 2, 1);
INSERT INTO saisie_categoriepanneau (id, label, parent_id, lft, rght, tree_id, level) VALUES (5, 'Danger Type G', 3, 4, 5, 2, 1);
INSERT INTO saisie_categoriepanneau (id, label, parent_id, lft, rght, tree_id, level) VALUES (3, 'Dangers', NULL, 1, 6, 2, 0);
INSERT INTO saisie_categoriepanneau (id, label, parent_id, lft, rght, tree_id, level) VALUES (6, 'Balises de sécurité', NULL, 1, 2, 1, 0);
INSERT INTO saisie_categoriepanneau (id, label, parent_id, lft, rght, tree_id, level) VALUES (9, 'Intersection', 8, 2, 3, 4, 1);
INSERT INTO saisie_categoriepanneau (id, label, parent_id, lft, rght, tree_id, level) VALUES (10, 'Priorité', 8, 4, 5, 4, 1);
INSERT INTO saisie_categoriepanneau (id, label, parent_id, lft, rght, tree_id, level) VALUES (13, 'Interdiction', 12, 5, 6, 6, 2);
INSERT INTO saisie_categoriepanneau (id, label, parent_id, lft, rght, tree_id, level) VALUES (14, 'Stationnement', 12, 7, 8, 6, 2);
INSERT INTO saisie_categoriepanneau (id, label, parent_id, lft, rght, tree_id, level) VALUES (15, 'Fin d''interdiction', 12, 3, 4, 6, 2);
INSERT INTO saisie_categoriepanneau (id, label, parent_id, lft, rght, tree_id, level) VALUES (12, 'Prescription Type B', 11, 2, 11, 6, 1);
INSERT INTO saisie_categoriepanneau (id, label, parent_id, lft, rght, tree_id, level) VALUES (16, 'Zone 30', 12, 9, 10, 6, 2);
INSERT INTO saisie_categoriepanneau (id, label, parent_id, lft, rght, tree_id, level) VALUES (11, 'Prescriptions', NULL, 1, 12, 6, 0);
INSERT INTO saisie_categoriepanneau (id, label, parent_id, lft, rght, tree_id, level) VALUES (8, 'Intersections et Prioritées', NULL, 1, 6, 4, 0);
INSERT INTO saisie_categoriepanneau (id, label, parent_id, lft, rght, tree_id, level) VALUES (7, 'Virages', NULL, 1, 2, 7, 0);
INSERT INTO saisie_categoriepanneau (id, label, parent_id, lft, rght, tree_id, level) VALUES (18, 'Obligation Type B', 17, 2, 7, 5, 1);
INSERT INTO saisie_categoriepanneau (id, label, parent_id, lft, rght, tree_id, level) VALUES (17, 'Obligations', NULL, 1, 8, 5, 0);
INSERT INTO saisie_categoriepanneau (id, label, parent_id, lft, rght, tree_id, level) VALUES (19, 'Obligation', 18, 5, 6, 5, 2);
INSERT INTO saisie_categoriepanneau (id, label, parent_id, lft, rght, tree_id, level) VALUES (20, 'Fin d''obligation', 18, 3, 4, 5, 2);