import os
ADMIN_DIR = os.path.dirname(__file__)

DEBUG = True
TEMPLATE_DEBUG = DEBUG
GALLERY_SAMPLE_SIZE = 1
PAGINATION_DEFAULT_PAGINATION = 10

INTERNAL_IPS = (
	'127.0.0.1',
)

ADMINS = (
	('Chedi Toueiti', 'chedi.toueiti@gmail.com'),
)

AUTH_PROFILE_MODULE = 'tajhiz.saisie.Utilisateurs'

MANAGERS = ADMINS

DATABASES = {
	'default': {
		'ENGINE'  : 'postgresql_psycopg2',
		'NAME'    : 'tajhiz',
		'USER'    : 'postgres',
		'PASSWORD': '',
		'HOST'    : 'tajhiz.3b.zzz',
		'PORT'    : '5432',
	}
}

TIME_ZONE          = 'Africa/Tunis'
LANGUAGE_CODE      = 'fr-FR'
SITE_ID            = 1
USE_I18N           = True
USE_L10N           = True
MEDIA_ROOT         = '/var/www/django/tajhiz/media/'
MEDIA_URL          = 'http://tajhiz.3b.zzz/media/'
ADMIN_MEDIA_ROOT   = '/var/www/django/tajhiz/media/Admin/'
ADMIN_MEDIA_PREFIX = 'http://tajhiz.3b.zzz/media/Admin/'
POSTGIS_VERSION    = (1, 5, 1)
STATICFILES_DIRS   = ()
DEFAULT_CHARSET    = 'utf-8'

SECRET_KEY = 'zzt-#n)6d8dl4pqq**=x&!b=sm2zp3m6h@nj5w#45pky60&_q4'

from django.conf.global_settings import TEMPLATE_CONTEXT_PROCESSORS

TEMPLATE_CONTEXT_PROCESSORS += (
	'django.core.context_processors.auth',
	'django.core.context_processors.debug',
	'django.core.context_processors.i18n',
	'django.core.context_processors.media',
	'django.contrib.messages.context_processors.messages',
	'django.core.context_processors.request',
) 

TEMPLATE_LOADERS = (
	'django.template.loaders.filesystem.Loader',
	'django.template.loaders.app_directories.Loader',
)

MIDDLEWARE_CLASSES = (
	'django.middleware.common.CommonMiddleware',
	'django.contrib.sessions.middleware.SessionMiddleware',
	'django.middleware.csrf.CsrfViewMiddleware',
	'django.contrib.auth.middleware.AuthenticationMiddleware',
	'django.contrib.messages.middleware.MessageMiddleware',
	'pagination.middleware.PaginationMiddleware',
	'debug_toolbar.middleware.DebugToolbarMiddleware',
)

ROOT_URLCONF = 'tajhiz.urls'

TEMPLATE_DIRS = (
	os.path.join(ADMIN_DIR, 'templates'),
)

INSTALLED_APPS = (
	'admin_tools',
	'admin_tools.theming',
	'admin_tools.menu',
	'admin_tools.dashboard',
	'django.contrib.auth',
	'django.contrib.contenttypes',
	'django.contrib.sessions',
	'django.contrib.sites',
	'django.contrib.messages',
	'django.contrib.gis',
	'django.contrib.admin',
	'tajhiz.saisie',
	'photologue',
	'tagging',
	'contact_form',
	'mptt',
	'debug_toolbar',
	'pagination',
)

DEBUG_TOOLBAR_CONFIG = {
	'INTERCEPT_REDIRECTS': False,
	'HIDE_DJANGO_SQL'    : False,
}