import logging
from tajhiz.saisie.choix      import *
from tajhiz.saisie.utils      import *
from django.contrib.gis.db    import models
from django.conf              import settings
from mptt.models              import MPTTModel
from mptt.models              import TreeForeignKey

from django.contrib.auth.models import User
from photologue.models import Photo
from photologue.models import Gallery

logger = logging.getLogger(__name__)

class Activite(models.Model):
	user   = models.ForeignKey    ( User )
	end    = models.DateTimeField (      )
	start  = models.DateTimeField (      )
	action = models.TextField     (      )
	closed = models.BooleanField  (      )
	def __unicode__(self):
		return "%s (%s -> %s) in %s" % (self.user.get_full_name(), self.start, self.end, ACTIONS_CHOICES[self.action][1])

class Telephone(models.Model):
    numero      = models.CharField( max_length = 40                          )
    type        = models.CharField( max_length = 20, choices = TYPES_CHOICES )
    commentaire = models.TextField( blank      = True                        )
    def __unicode__(self):
    	return u"[%s] : %s" % (self.type, self.numero)

class Utilisateur(models.Model):
	user       = models.ForeignKey           ( User, unique = True         )
	telephones = models.ManyToManyField      ( Telephone                   )
	civilite   = models.PositiveIntegerField ( choices    = CIVILITE_TYPES )
	fonction   = models.CharField            ( max_length = 50             )
	addresse   = models.TextField            ( blank = True, null = True   )
	def __unicode__(self):
		return u"%s %s" % (CIVILITE_TYPES[self.civilite-1][1], self.user.get_full_name())

class DirectionRegionale(models.Model):
	objects      = models.GeoManager        (                                )
	label        = models.CharField         ( max_length = 50, unique = True )
	description  = models.TextField         ( blank = True, null = True      )
	geom         = models.MultiPolygonField ( srid       = 4326              )
	utilisateurs = models.ManyToManyField   ( Utilisateur, null = True       )
	def __unicode__(self):
		return self.label
	class Meta:
		verbose_name_plural = 'Directions Regionales'

class GalleryUserOwnership(models.Model):
	locked   = models.BooleanField ( default=False                       )
	owner    = models.ForeignKey   ( User, default=1                     )
	gallery  = models.ForeignKey   ( Gallery                             )
	progress = models.IntegerField ( default=0, choices=GALLERY_PROGRESS )
	def __unicode__(self):
		return u"%s %s by %s" % (self.gallery.title, "locked" if self.locked else "owned" ,self.owner.get_full_name())
	class Meta:
		verbose_name_plural = u"Galleries ownership"

class Media(models.Model):
	image        = models.ForeignKey    ( Photo                                  )
	audio        = models.TextField     (                                        )
	image_valide = models.IntegerField  ( default    = 1, choices=MEDIA_VALIDITY )
	audio_valide = models.IntegerField  ( default    = 1, choices=MEDIA_VALIDITY )

	def image_container(self):
		return self.image.admin_thumbnail()
	
	def audio_container(self):
		audio_tag_error = u'Your browser does not support the audio element.'
		return u'''
					<audio controls="controls">
						%s
						<source src="%s.ogg" type="audio/ogg">
						<source src="%s.mp3" type="audio/mpeg">
					</audio>
				''' % (
					audio_tag_error, 
					settings.MEDIA_URL + self.audio.replace(settings.MEDIA_ROOT, ''), 
					settings.MEDIA_URL + self.audio.replace(settings.MEDIA_ROOT, ''))
	
	def __unicode__(self):
		return u"(%s | %s)-(%s | %s)" % (self.image_valide, self.audio_valide, self.image, self.audio)

	image_container.allow_tags = True
	audio_container.allow_tags = True
	class Meta:
		permissions = (
			('can_qualify' , 'can qualify image'               ),
			('can_rotate'  , 'can rotate image'                ),
			('can_validate', 'can validate image qualification'),
			('can_unlock'  , 'can release gallery lock'        ),
			('can_taint'   , 'can mark gallery as dirty'       ),
		)

class Anomalie(models.Model):
	type        = models.IntegerField ( choices = ANOMALIES_TYPES )
	description = models.TextField    (                           )
	def __unicode__(self):
		return u"[%s] : %s" % (self.type, self.description)

class Observation(models.Model):
	texte = models.TextField()
	def __unicode__(self):
		return self.texte

class Voie(models.Model):
	objects   = models.GeoManager      (                                       )
	geom      = models.LineStringField ( srid       = 4326                     )
	label     = models.CharField       ( max_length = 50                       )
	nom_usage = models.CharField       ( max_length = 50                       )
	largeur   = models.IntegerField    ( choices    = LARGEUR_VOIES            )
	cycle     = models.IntegerField    ( choices    = CYCLE_TYPES              )
	type      = models.CharField       ( max_length = 10 ,choices = TYPE_VOIES )
	def __unicode__(self):
		return self.label

class Collecte(models.Model):
	objects  = models.GeoManager           (                           )
	date     = models.DateField            (                           )
	equipe   = models.PositiveIntegerField ( choices = EQUIPES_CHOICES )
	voie     = models.ManyToManyField      ( Voie, null = True         )
	sens     = models.TextField            (                           )
	def __unicode__(self):
		return u"[%s][%s]" % (self.date, EQUIPES_CHOICES[self.equipe-1][1])

class Tracking(models.Model):
	objects  = models.GeoManager           (                   )
	gpx_file = models.TextField            (                   )
	parcour  = models.MultiLineStringField ( srid       = 4326 )
	collecte = models.ForeignKey           ( Collecte          )

class Event(models.Model):
	objects    = models.GeoManager    (          )
	position   = models.PointField    (          )
	date       = models.DateTimeField (          )
	sens       = models.BooleanField  (          )
	collecte   = models.ForeignKey    ( Collecte )
	media      = models.OneToOneField ( Media    )
	def __unicode__(self):
		return u"(%s, %s)[%s](%s)" % (self.position.x, self.position.y, '+' if self.sens else '-', date)

class SignalRoutier(models.Model):
	objects      = models.GeoManager           (                                  )
	medias       = models.ForeignKey           ( Media                            )
	position     = models.PointField           ( srid    = 4326                   )
	etat         = models.PositiveIntegerField ( choices = ETATS                  )
	type         = models.PositiveIntegerField ( choices = SIGNALS_ROUTIERS_TYPES )
	def __unicode__(self):
		return u"(%s, %s)" % ( self.position.x, self.position.y )
	class Meta:
		verbose_name_plural = 'Signaux Routier'

class Support(models.Model):
	type    = models.PositiveIntegerField ( choices = SUPPORT_TYPE )
	hauteur = models.PositiveIntegerField (                        )
	etat    = models.PositiveIntegerField ( choices = ETATS        )
	signal  = models.ForeignKey           ( SignalRoutier          )
	def __unicode__(self):
		return u"[%s][%s] : %s" % (type, hauteur, signal)

class Noeud(models.Model):
	objects  = models.GeoManager (                                       )
	label    = models.CharField  ( max_length = 100                      )
	type     = models.CharField  ( max_length = 1, choices = NOEUD_TYPES )
	position = models.PointField ( srid=4326                             )
	def __unicode__(self):
		return self.label

class TextePanneau(models.Model):
	langue = models.PositiveIntegerField ( choices = LANGUES_TYPES )
	texte  = models.TextField            (                         )
	def __unicode__(self):
		return self.texte

class Panonceau(models.Model):
	code_info = models.CharField ( max_length = 40, unique = True )
	def __unicode__(self):
		return self.code_info
	class Meta:
		verbose_name_plural = 'panonceaux'

class Fabriquant(models.Model):
	label       = models.CharField       ( max_length = 50 )
	utilisateur = models.ManyToManyField ( Utilisateur     )
	def __unicode__(self):
		return self.label

class CategoriePanneau(MPTTModel):
	label  = models.CharField ( max_length = 40, unique = True    )
	parent = TreeForeignKey('self', null=True, blank=True, related_name='SousCategories')
	def __unicode__(self):
		return self.label
	class MPTTMeta:
		order_insertion_by = ['label']
	class Meta:
		verbose_name_plural = 'Categories de panneau'

class CodeInfo(models.Model):
	label        = models.CharField       ( max_length = 20, unique = True )
	categorie    = models.ForeignKey      ( CategoriePanneau               )
	informations = models.ManyToManyField ( TextePanneau                   )
	def __unicode__(self):
		return "[%s] %s" % (self.label, self.informations.all())

class Panneau(models.Model):
	signal       = models.ForeignKey           ( SignalRoutier                )
	rang         = models.PositiveIntegerField (                              )
	rouille      = models.BooleanField         (                              )
	fabriquant   = models.ForeignKey           ( Fabriquant                   )
	sens         = models.PositiveIntegerField ( choices = SENS_TYPES         )
	etat         = models.PositiveIntegerField ( choices = ETATS              )
	forme        = models.PositiveIntegerField ( choices = PANNEAU_FORMES     )
	dimension    = models.PositiveIntegerField ( choices = PANNEAU_DIMENSIONS )
	revetement   = models.PositiveIntegerField ( choices = REVETEMENT_TYPES   )
	lisibilite   = models.PositiveIntegerField ( choices = LISIBILITE_TYPES   )
	panonceaux   = models.ManyToManyField      ( Panonceau   , null = True    )

class IndicationDirection(models.Model):
	sens         = models.PositiveIntegerField ( choices = SENS_TYPES      )
	distance     = models.PositiveIntegerField ( blank = True, null = True )
	destinations = models.ManyToManyField      ( TextePanneau              )
	def __unicode__(self):
		return u"%s (%s Km)(%s)" % (self.destinations.all(), self.distance, '+' if self.sens == 1 else '-' )
	class Meta:
		verbose_name_plural = 'Indications de direction'

class AbstractPanneauUniDirectionnel(Panneau):
	direction = models.ForeignKey ( IndicationDirection )
	class Meta:
		abstract = True

class AbstractPanneauMulDirectionnel(Panneau):
	direction = models.ManyToManyField ( IndicationDirection )
	class Meta:
		abstract = True

class PanneauAnomalie          ( Anomalie    ) : panneau        = models.ForeignKey ( Panneau       )
class SignalRoutierAnomalies   ( Anomalie    ) : signal_routier = models.ForeignKey ( SignalRoutier )
class PanneauObservation       ( Observation ) : panneau        = models.ForeignKey ( Panneau       )
class SignalRoutierObservation ( Observation ) : signal_routier = models.ForeignKey ( SignalRoutier )

class PanonceauxCodeInfo           (CodeInfo) : pass
class PanneauPoliceCodeInfo        (CodeInfo) : pass
class PanneauRegistreCodeInfo      (CodeInfo) : pass
class PanneauDirectionnelCodeInfo  (CodeInfo) : pass
class PanneauDiagramatiqueCodeInfo (CodeInfo) : pass

class PanneauPolice        ( Panneau                        ) : code_info = models.ForeignKey ( PanneauPoliceCodeInfo        )
class PanneauRegistre      ( AbstractPanneauMulDirectionnel ) : code_info = models.ForeignKey ( PanneauRegistreCodeInfo      )
class PanneauDirectionnel  ( AbstractPanneauUniDirectionnel ) : code_info = models.ForeignKey ( PanneauDirectionnelCodeInfo  )
class PanneauDiagramatique ( AbstractPanneauMulDirectionnel ) : code_info = models.ForeignKey ( PanneauDiagramatiqueCodeInfo )