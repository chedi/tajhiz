jQuery.fn.jSignalSignals = function (aObj, aSignal)
{
	this.signal = aSignal;
	this.obj    = aObj;
	this.slots  = new Array();
	
	this.addSlot = function(aObj, aSlot)
	{
		var slot = null;
		if(jQuery.jSignal.isSignal(aSlot)) {
			slot = jQuery.jSignal.getSignal(aSlot);
		}
		else {
			slot = new jQuery.fn.jSignalSignals(aObj, aSlot);
			jQuery.jSignal.addSignal(slot);
		}

		this.slots.push(slot);
	}

	this.removeSlot = function(aObj, aSlot)
	{
		if(jQuery.jSignal.isSignal(aSlot)) {
			var slot = jQuery.jSignal.getSignal(aSlot);

			for(var i=0; i<this.slots.length; i++) {
				if(this.slots[i] == slot) {
					jQuery.jSignal.removeSignal(slot);
					this.slots.splice(i, 1)

					if(this.slots.length == 0)
						this.clean();

					return true;
				}
			}
		}
		else
			return false;
	}
	
	this.clean = function ()
	{
		jQuery.jSignal.removeSignal(this);
			
		this.signal = null;
		this.obj    = null;
		this.slots  = new Array();
	}
}

jQuery.fn.jSignal = function () {
	this.mSignals = new Array();

	this.connect = function (aObj1, aSignal, aObj2, aSlot)
	{
		if(!this.isSignal(aSignal))
			this.addSignal(new jQuery.fn.jSignalSignals(aObj1, aSignal));

		this.getSignal(aSignal).addSlot(aObj2, aSlot);
	}

	this.disconnect = function  (aObj1, aSignal, aObj2, aSlot)
	{
		this.getSignal(aSignal).removeSlot(aObj2, aSlot);
	}

	this.addSignal = function  (oSignal)
	{
		this.mSignals.push(oSignal);
	}
	
	this.removeSignal = function  (oSignal)
	{
		for(var i=0; i<this.mSignals.length; i++) {
			if(this.mSignals[i] == oSignal)
				this.mSignals.splice(i, 1);
		}
	}
	
	this.isSignal = function  (aSignal)
	{
		for(var a in this.mSignals) {
			if(this.mSignals[a].signal == aSignal)
				return true;
		}
		
		return false;
	}

	this.getSignal = function  (aSignal)
	{
		for(var a in this.mSignals) { 
			if(this.mSignals[a].signal == aSignal)
				return this.mSignals[a];
		}
		return null;
	}

	this.emit = function  (aSignal)
	{
		for(var i in this.mSignals) {
			if(this.mSignals[i].signal == aSignal) { 
				var oSignal = this.mSignals[i];
				
				var aStr = '';
				if(arguments.length > 1) {
					for(var a=1; a<arguments.length; a++)
						aStr += ', arguments['+a+']';
				}
				var sStr = '';
				sStr += '(oSignal.signal).call(oSignal.obj';
				sStr += aStr;
				sStr += ');';
				sStr += 'for(var y in oSignal.slots) {';
				sStr +=     'this.emit(oSignal.slots[y].signal'+aStr+');';
				sStr += '}';
				
				eval(sStr);
			}
		}
	}
}

jQuery.jSignal = new jQuery.fn.jSignal();