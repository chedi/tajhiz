import os
import random
import sndhdr

from PIL                          import Image
from django.conf                  import settings
from django.contrib.gis.db        import models
from django.utils.safestring      import mark_safe
from django.utils.translation     import ugettext as _
from django.contrib.auth.models   import get_hexdigest
from django.contrib.admin.widgets import AdminFileWidget

def thumbnail(image_path):
    return u'<img src="%s" alt="%s" width="200" height="200" />' % (image_path, image_path)

class AdminImageWidget(AdminFileWidget):
    def render(self, name, value, attrs=None):
        output     = []
        file_name  = '%s%s' % ('EventImage/', str(value))
        error_pic  = '%s%s' % ('EventImage/', 'not_found.jpg')
        file_url   = '%s%s' % (settings.MEDIA_URL , file_name)
        file_path  = '%s%s' % (settings.MEDIA_ROOT, file_name)
        error_url  = '%s%s' % (settings.MEDIA_URL , error_pic)
        error_path = '%s%s' % (settings.MEDIA_ROOT, error_pic)
        if file_name:
            try:
                Image.open(file_path)
                output.append('<a target="_blank" href="%s">%s</a>' % ( file_url,  thumbnail(file_url)  ))
            except IOError:
                output.append('<a target="_blank" href="%s">%s</a>' % ( error_url, thumbnail(error_url) ))
        return mark_safe(u''.join(output))

class AudioFileWidget(AdminFileWidget):
    def render(self, name, value, attrs = None):
        output = []
        file_name = str(value)
        file_url  = '%s%s%s' % (settings.MEDIA_URL , 'EventAudio/', file_name)
        file_path = '%s%s%s' % (settings.MEDIA_ROOT, 'EventAudio/', file_name)
        audio_tag_error = _('Your browser does not support the audio element.')
        error_output = '%s (not found or not a valid audio file !!) %s' % (file_name, file_url)
        if file_name:
            try:
                #rs = sndhdr.what(file_path)
                #if(rs == None):
                #    output.append(error_output)
                #else:
                output.append('<audio src="%s" controls="controls">%s</audio>' % (file_url, audio_tag_error))
            except IOError:
                output.append(error_output)
        #output.append(super(AdminFileWidget, self).render(name, value, attrs))
        return mark_safe(u''.join(output))

class SelfForeignKey(models.ForeignKey):
    def pre_save(self, instance, add):
        manager = instance.__class__.objects
        ancestor_id = getattr(instance, self.attname)
        while ancestor_id is not None:
            if ancestor_id == instance.id:
                return None
            ancestor = manager.get(id=ancestor_id)
            ancestor_id = getattr(ancestor, self.attname)
        return getattr(instance, self.attname)

class Sha1PasswordHash(models.CharField):
    def pre_save(self, instance, add):
    	algo = 'sha1'
        password = getattr(instance, self.attname)
        salt = get_hexdigest(algo, str(random.random()), str(random.random()))[:5]
        hsh  = get_hexdigest(algo, salt, password)
        return '%s%s' % (salt, hsh)