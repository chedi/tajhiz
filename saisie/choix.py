ETATS = (
	( 3, 'Mauvais' ),
	( 2, 'Moyen'   ),
	( 1, 'Bon'     )
)

SENS_TYPES = (
	( 1, 'Sens direct'   ),
	( 2, 'Sens indirect' )
)

TYPES_CHOICES = (
	( 'HOME'    , 'Home'       ),
	( 'WORK'    , 'Work'       ),
	( 'MOBILE'  , 'Mobile'     ),
	( 'HOME_FAX', 'Fax (home)' ),
	( 'WORK_FAX', 'Fax (work)' ),
	( 'PAGER'   , 'Pager'      ),
	( 'OTHER'   , 'Other'      )
)

UTILISATEURS_ROLES = (
	( True , 'Administrateur' ),
	( False, 'Utilisateur'    )
)

UTILISATEURS_ETATS = (
	( True , 'Actif'   ),
	( False, 'Inactif' )
)

MEDIA_VALIDE = (
	( True , 'Valide'   ),
	( False, 'Invalide' )
)

ANOMALIES_TYPES = (
	( 0, 'Anomalie 1' ),
	( 1, 'Anomalie 2' ),
	( 3, 'Anomalie 3' )
)

LARGEUR_VOIES = (
	( 1, '2*1 voie' ),
	( 2, '2*2 voie' )
)

CYCLE_TYPES = (
	( 1, 'Cycle Urbain'       ),
	( 2, 'Cycle Extra-urbain' ),
	( 3, 'Cycle Mixte'        )
)

SIGNALS_ROUTIERS_TYPES = (
	( 1, 'Police'       ),
	( 2, 'Directionnel' ),
	( 3, 'Mixte'        )
)

SUPPORT_TYPE = (
	( 1, 'Panneau Police Simple'            ),
	( 2, 'Panneau Police Combine I'         ),
	( 3, 'Panneau Police Combine II'        ),
	( 4, 'Panneau Direction Portique'       ),
	( 5, 'Panneau Direction Potence'        ),
	( 6, 'Panneau Direction Haut-mat'       ),
	( 7, 'Panneau Direction Potence double' )
)

NOEUD_TYPES = (
	('l', 'L'),
	('o', 'O'),
	('t', 'T'),
	('x', 'X'),
	('y', 'Y')
)

LANGUES_TYPES = (
	(1, 'Arabe'),
	(2, 'Francais')
)

PANNEAU_FORMES = (
	( 1, 'Simple face' ),
	( 2, 'Double face' )
)

PANNEAU_DIMENSIONS = (
	( 1, 'Miniature'  ),
	( 2, 'Petit'      ),
	( 3, 'Moyen'      ),
	( 4, 'Grand'      ),
	( 5, 'Tres grand' ),
	( 6, 'Standard'   )
)

REVETEMENT_TYPES = (
	( 1, 'DG'    ),
	( 2, 'EG'    ),
	( 3, 'Mixte' )
)

LISIBILITE_TYPES = (
	( 1, 'Lisibilite'         ),
	( 2, 'Moyenement Lisible' ),
	( 3, 'Peu Lisible'        ),
	( 4, 'Non Lisible'        )
)

CIVILITE_TYPES = (
	( 1, 'Monsieur'     ),
	( 2, 'Madame'       ),
	( 3, 'Mademoiselle' )
)

EQUIPES_CHOICES = (
	( 1, 'Equipe 1 (Aymen  + Farid)' ),
	( 2, 'Equipe 2 (Hichem + I9bel)' ),
)

TYPE_VOIES = (
	( 1, 'RR'       ),
	( 2, 'RL'       ),
	( 3, 'RN'       ),
	( 4, 'Piste'    ),
	( 5, 'Autoroute'),
)

IMPORT_STATUS = (
	( True, 'Success'  ),
	( False, 'Failure' ),
)

MEDIA_VALIDITY = (
	( 1, 'Valide'  ),
	( 2, 'Invalide'),
	( 3, 'Mehhhhhh'),
)

GALLERY_PROGRESS = (
	( 0, 'Virgin'    ),
	( 1, 'Dirty'     ),
	( 2, 'Completed' ),
)